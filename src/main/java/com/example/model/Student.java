package com.example.model;

import javax.persistence.ManyToOne;

public class Student 
{
	private int studentId;
	
	private String name;
	
	private String address;
	
	private long phone;
	
	private String DOB;
	
	private String email;
	
	private String username;
	
	private String password;
	
	@ManyToOne
	private Course course;
	
	

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String dOB) {
		DOB = dOB;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", name=" + name + ", address=" + address + ", phone=" + phone
				+ ", DOB=" + DOB + ", email=" + email + ", username=" + username + ", password=" + password
				+ ", course=" + course + "]";
	}

	public Student(int studentId, String name, String address, long phone, String dOB, String email, String username,
			String password, Course course) {
		super();
		this.studentId = studentId;
		this.name = name;
		this.address = address;
		this.phone = phone;
		DOB = dOB;
		this.email = email;
		this.username = username;
		this.password = password;
		this.course = course;
	}

	public Student() {
		super();
	}

	
	
	
	
}

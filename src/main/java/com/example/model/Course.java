package com.example.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

public class Course 
{
	private int courseId;
	private String CourseDescription;
	
	@OneToMany(mappedBy="course",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private List<Student> students=new ArrayList<Student>();
	
	
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	public String getCourseDescription() {
		return CourseDescription;
	}
	public void setCourseDescription(String courseDescription) {
		CourseDescription = courseDescription;
	}
	public Course() {
		super();
	}
	public List<Student> getStudents() {
		return students;
	}
	public void setStudents(List<Student> students) {
		this.students = students;
	}
	@Override
	public String toString() {
		return "Course [courseId=" + courseId + ", CourseDescription=" + CourseDescription + ", students=" + students
				+ "]";
	}
	public Course(int courseId, String courseDescription, List<Student> students) {
		super();
		this.courseId = courseId;
		CourseDescription = courseDescription;
		this.students = students;
	}
	
	
	
}

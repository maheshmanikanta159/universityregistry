package com.example.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

import com.example.model.Course;

@Service
public class CourseService implements CourseDao
{
	
	Course c1=new Course(101,"BTech",null);
	Course c2=new Course(102,"BDS",null);
	Course c3=new Course(103,"BBA",null);
	Course c4=new Course(104,"MBBS",null);
	
	List<Course> cl=new ArrayList<Course>();
	
	public CourseService() {
		super();
		cl.add(c1);
		cl.add(c2);
		cl.add(c3);
		cl.add(c4);
	}

	public Course add(Course c)
	{
		cl.add(c);
		return c;
	}
	 
	public Course getCourse(Integer courseId)
	{
		for(Course c:cl)
		{
			if(c.getCourseId()==courseId)
			{
				return c;
			}
		}
		return null;
	}
	 
	 public List<Course> listCourse()
	 {
		 return cl;
	 }
	 
	 public Course delete(Integer courseId)
	 {
		 Course cr=new Course();
		 for(Course c:cl)
		 {
			if(c.getCourseId()==courseId)
			{
				cr=c;
				cl.remove(cl.indexOf(c));
			}
		}
		 return cr;
	 }
	 
	 public Course update(Integer courseId, Course c1)
	 {
		 for(Course c:cl)
		 {
			if(c.getCourseId()==courseId)
			{
				int x=cl.indexOf(c);
				cl.remove(x);
				cl.add(x, c1);
			}
		}
		 return c1; 
	 }
	 
	 public Course getCourseById(int id)
	 {
		 Course cr=new Course();
		 for(Course c:cl)
		 {
			if(c.getCourseId()==id)
			{
				int x=cl.indexOf(c);
				cr=cl.get(x);
			}
		}
		 return cr;
	 }
}

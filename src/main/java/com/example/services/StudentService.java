package com.example.services;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

import com.example.model.Course;
import com.example.model.Student;

@Service
public class StudentService implements StudentDao
{
	CourseService cs=new CourseService();
	Student stu1=new Student(1,"Mahesh", "Macherla", 1234567890, "12/10/2002","abc@gmail.com","Mahesh1","1234",cs.c1);
	Student stu2=new Student(2,"Sanjay", "Macherla", 1234567898, "06/12/1999","abcde@gmail.com","Sanju","12345",cs.c2);
	Student stu3=new Student(3,"Bhargav", "Guntur", 1234567899, "06/01/2000","abcdef@gmail.com","Bhargav1","123456",cs.c3);
	List<Student> students=new ArrayList<Student>();
	public StudentService()
	{
		students.add(stu1);
		students.add(stu2);
		students.add(stu3);
	}
	public Student add(Student s)
	{
		Course c=cs.getCourse(s.getCourse().getCourseId());
		s.setCourse(c);
		students.add(s);
		return s;
	}
	 
	 public Student getStudent(Integer studentId)
	 {
		 for(Student s:students)
		 {
			 if(s.getStudentId()==studentId)
			 {
				 return s;
			 }
		 }
		 return null;
	 }
	 public List<Student> listStudents()
	 {
		 return students;
	 }
	 
	 public Student delete(Integer studentid)
	 {
		 Student sr=new Student();
		 for(Student s:students)
		 {
			 if(s.getStudentId()==studentid)
			 {
				 sr=s;
				 students.remove(students.indexOf(s));
			 }
		 }
		 return sr;
	 }
	 
	 public Student update(Integer studentId, Student s)
	 {
		 for(Student su:students)
		 {
			 if(su.getStudentId()==studentId)
			 {
				 int x=students.indexOf(su);
				 students.remove(x);
				 Course c=cs.getCourse(s.getCourse().getCourseId());
				 s.setCourse(c);
				 students.add(x,s);
			 }
		 }
		 return s;
	 }
}

package com.example.services;

import java.util.List;

import com.example.model.Student;

public interface StudentDao 
{
	 public Student add(Student s);
	 
	 public Student getStudent(Integer studentId);
	 
	 public List<Student> listStudents();
	 
	 public Student delete(Integer studentid);
	 
	 public Student update(Integer studentId, Student s);
}

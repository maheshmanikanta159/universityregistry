package com.example.services;

import java.util.List;

import com.example.model.Course;

public interface CourseDao 
{
	public Course add(Course s);
	 
	 public Course getCourse(Integer courseId);
	 
	 public List<Course> listCourse();
	 
	 public Course delete(Integer courseId);
	 
	 public Course update(Integer courseId, Course c);
}

package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Student;
import com.example.services.StudentService;

@RestController
@RequestMapping("/student")
public class StudentController 
{
	@Autowired
	private StudentService ss;
	
	@RequestMapping(method=RequestMethod.POST,value="/add")
	private Student add(@RequestBody Student s)
	{
		return ss.add(s);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/list/{id}")
	private Student showone(@PathVariable int id)
	{
		return ss.getStudent(id);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/list")
	private List<Student> showall()
	{
		return ss.listStudents();
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/update/{id}")
	private Student update(@RequestBody Student s,@PathVariable int id)
	{
		return ss.update(id, s);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/delete/{id}")
	private Student delete(@PathVariable int id)
	{
		return ss.delete(id);
	}
}

package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Course;
import com.example.services.CourseService;

@RestController
@RequestMapping("/course")
public class CourseController 
{
	@Autowired
	CourseService cs;
	
	@RequestMapping(method=RequestMethod.POST,value="/add")
	private Course add(@RequestBody Course c)
	{
		return cs.add(c);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/list/{id}")
	private Course showone(@PathVariable int id)
	{
		return cs.getCourse(id);
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/list")
	private List<Course> showall()
	{
		return cs.listCourse();
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/update/{id}")
	private Course update(@RequestBody Course c,@PathVariable int id)
	{
		return cs.update(id, c);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/delete/{id}")
	private Course delete(@PathVariable int id)
	{
		return cs.delete(id);
	}
}
